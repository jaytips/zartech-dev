<!DOCTYPE html>
<html lang="en">
<head>
  <title>Zartech Services</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
  <!-- link for the icons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <!-- link for the CSS -->
  <link rel="stylesheet" type="text/css" href="{{('css/style.css')}}">
</head>
<body>
    
    <div class="container-fluid">
        <!-- main navbar (grey background/ fixed position) -->
        <div class="row">
            <div class="col-sm-12">
                 <nav class="navbar navbar-expand-sm bg-dark navbar-dark fixed-top" style="height: 9%;">
                    <ul class="navbar-nav">
                        <li class="nav-item">
                        <a class="nav-link" href="#"><img src="{{('images/logo.png')}}" alt="Zartech"></a>
                        </li>
                        <li class="nav-item">
                        <a class="nav-link" href="#"></a>
                        </li>
                    </ul>

                    <!-- search box -->
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Search Item..">
                        <div class="input-group-append">
                        <button class="btn btn-secondary" type="button">
                            <i class="fa fa-search"></i>
                        </button>
                        </div>
                    </div> <!--end of search box -->
                </nav>
            </div>
       </div>  <!--end of 2nd navbar (grey background / fixed position)-->
       
       <!-- 2nd navbar (yellow background) -->
        <div class="row" id="nav2">
            <div class="col-sm-2" >
                <nav class="navbar">
                    <b>Follow us on:</b>
                    <a href="https://facebook.com/" target="_blank"><i class="fa fa-facebook-square" style="font-size:22px;color:#343A40"></a></i>
                    <a href="https://youtube.com/" target="_blank"><i class="fa fa-youtube-square" style="font-size:22px;color:#343A40"></a></i>
                    <a href="https://twitter.com/" target="_blank"><i class="fa fa-twitter-square" style="font-size:22px;color:#343A40"></a></i>
                </nav>
            </div>
            <div class="col-sm-7">
                <!-- Intended to be empty -->
                <!-- for spacing purposes -->
            </div>
            <div class="col-sm-3" style="float: right;">
                <nav class="navbar" id="navb2">
                    <!-- DONT REMOVE span badge for notification. but not yet final. -->
                    <!-- <span class="badge badge-light">4</span> -->
                    <a href="#"><i class="fa fa-bell" style="font-size:20px;color:#343A40"><span class="label"> notification</span></i></a>
                    <a href="#"><i class="fa fa-question-circle" style="font-size:20px;color:#343A40"><span class="label"> help</span></i></a>
                    <div class="dropdown">
                        <i class="fa fa-user" style="font-size:20px;color:#343A40"> <span class="label"> login</span>
                            <div class="dropdown-content">
                                <a href="#">Login</a>
                                <a href="#">Register</a>
                            </div>
                        </i>
                    </div> <!--end of dropdown-->
                </nav>
            </div>  <!--end of column-->
        </div>  <!--end of 2nd navbar (yellow background)-->
    </div>

    <div class="container-fluid" id="content">
        <div class="row">
            <div class="col-sm-7">
                <!-- Intended to be empty -->
                <!-- for spacing purposes -->
            </div>
            <div class="col-sm-5">
                <h1><br> Lets build the future together with Zartech.</h1>
                <h4><br> You can now earn as much as you want through building your online store with <strong>Zartech Services</strong>.</h4>
            </div>
        </div> <!-- end of row -->
    </div> <!-- end of container-->

    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-4" >
                <!-- TO DO -->
            </div>
            <div class="col-sm-4">
                <!-- TO DO -->
            </div>
            <div class="col-sm-4">
                <!-- TO DO -->
            </div>
        </div>
    </div>
</body>
</html>